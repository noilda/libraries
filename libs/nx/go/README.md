# @noilda/nx-go

## Table of Contents

- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Usage
`Work in progress`

## Maintainers

[@adlion](https://gitlab.com/adlion)

## Contributing

Feel free to dive in! [Open an issue](https://gitlab.com/noilda/libraries/-/issues) or submit PRs.

## License

MIT © 2022 Noilda