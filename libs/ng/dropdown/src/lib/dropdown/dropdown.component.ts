import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  Renderer2,
  TemplateRef,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'noilda-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DropdownComponent implements AfterViewInit {
  @Input() items: Array<string | number> = [];
  @Input() selected: string | number = '';
  @Output() emitSelected = new EventEmitter<string | number>();
  @ViewChild('dpItems', { read: ElementRef, static: true })
  dpItems!: ElementRef;
  @ContentChild('beforeRef',{static:false}) beforeTmplRef!: TemplateRef<any>;
  @ContentChild('afterRef',{static:false}) afterTmplRef!: TemplateRef<any>;
  constructor(private renderer: Renderer2) {}

  private elObserver!: IntersectionObserver;
  private windowListener: any;

  ngAfterViewInit(): void {
    this.selected = this.items[0];
  }

  onDropDownClick() {
    this.dpItems.nativeElement.classList.add('dropdown__items-up');
    
    this.interSectionObserver();

    setTimeout(() => {
      this.windowListener = this.renderer.listen('window', 'click', () => {
        this.unObserve();
        this.windowListener();
      });
    }, 250);
  }

  private interSectionObserver() {
    this.elObserver = new IntersectionObserver(
      (entries) => this.onAppears(entries),
      { threshold: 1 }
    );

    this.elObserver.observe(this.dpItems.nativeElement);
  }

  private onAppears(records: IntersectionObserverEntry[]) {
    for (const record of records) {
      const targetInfo = record.boundingClientRect;
      const rootBoundsInfo = record.rootBounds;
      if (rootBoundsInfo) {
        if (targetInfo.bottom >= rootBoundsInfo.bottom) {
          this.dpItems.nativeElement.classList.remove('dropdown__items-down');
          this.dpItems.nativeElement.classList.add('dropdown__items-up');
        }
        if (targetInfo.top < rootBoundsInfo.top) {
          this.dpItems.nativeElement.classList.remove('dropdown__items-up');
          this.dpItems.nativeElement.classList.add('dropdown__items-down');
        }
      }
    }
  }

 
  unObserve() {
    this.elObserver.unobserve(this.dpItems.nativeElement);
    this.dpItems.nativeElement.classList.remove('dropdown__items-up');
    this.dpItems.nativeElement.classList.remove('dropdown__items-down');
  }
}
