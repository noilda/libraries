import { Component } from '@angular/core';

@Component({
  selector: 'noilda-v-dropdown',
  templateUrl: './v-dropdown.component.html',
  styleUrls: ['./v-dropdown.component.scss'],
})
export class VDropdownComponent {
  constructor() {}

  items: Array<string | number> = ['Test 1', 'Test 2', 'Test 3', 'Test 4'];
  selected = this.items[0];

  onItemSelected(item: string | number) {
    this.selected = item;
  }
}
