import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { VLivePulsarComponent } from './views/v-live-pulsar/v-live-pulsar.component';
import { LivePulsarModule } from '@noilda/live-pulsar';
import { DropdownModule } from '@noilda/dropdown';
import { VDropdownComponent } from './views/v-dropdown/v-dropdown.component';
@NgModule({
  declarations: [AppComponent, VLivePulsarComponent, VDropdownComponent],
  imports: [
    BrowserModule,
    LivePulsarModule,
    DropdownModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
