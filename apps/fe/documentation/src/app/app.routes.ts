import { Route } from '@angular/router';
import { VDropdownComponent } from './views/v-dropdown/v-dropdown.component';
import { VLivePulsarComponent } from './views/v-live-pulsar/v-live-pulsar.component';

export const appRoutes: Route[] = [
  { path: 'live-pulsar', component: VLivePulsarComponent },
  { path: 'dropdown', component: VDropdownComponent },
];
